"use strict";




var express = require('express');
var passport = require('passport')
, LocalStrategy = require('passport-local').Strategy;
var crypto = require('./password.js');
var app = module.exports = express();

//Config
app.configure(function(){
   app.set('port', process.env.PORT || 3000);
   app.set('views', './app');
   app.engine('.html', require('ejs').renderFile);
   app.set('view engine', 'html');
   app.use(express.bodyParser());
   app.use(express.json());
   app.use(express.urlencoded());
   app.use(express.methodOverride());
   app.use( express.cookieParser() );
   app.use(express.session({ secret: 'anything' }));
   app.use(passport.initialize());
   app.use(passport.session());
   app.use(app.router);
   app.use(express.favicon());
   app.use(express.logger('dev'));
   app.use(express.static('./app'));
});





// Angular Routes
var routes = require('./routes/index.js');
app.get('/', routes.index);
app.get('/partials/:name', routes.partials);

//RESTful API
var api = require('./routes/api.js');
app.get('/api/skills', api.list);
app.post('/api/skill', api.addSkill);


passport.serializeUser(function(user, done) {
 done(null, user);
});

passport.deserializeUser(function(user, done) {
 done(null, user);
});


passport.use(new LocalStrategy(
 function(username, password, done) {
  api.findAUser({ username: username }, function(user) {
   console.log("trying to login...");
   if (user === null) {
    console.log("fail!");
    return done(null, false, { message: 'Incorrect username.' });
 }
 if(!crypto.validate(user.hash, password)) {
    console.log("fail!");
    return done(null, false, { message: 'Incorrect password.' });
 }
 console.log("success!");
 return done(null, user);
});
}
));


app.post('/login',
   passport.authenticate('local'),  function(req, res) {
    // If this function gets called, authentication was successful.
    // `req.user` contains the authenticated user.
    res.send("ok");
 });

app.get('/logout', function(req, res){
  req.logout();
  res.send("ok");
});

//Start server
var server = app.listen(app.get('port'), function(){
  console.log("Server listening on port %d", server.address().port);
});





