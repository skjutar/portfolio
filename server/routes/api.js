/* The API controller
   Exports 3 methods:
   * post - Creates a new thread
   * list - Returns a list of threads
   * show - Displays a thread and its posts
   */
   var fs = require('fs');
   var crypto = require('../password.js');
   var defaultSkillsPath = __dirname + "/skills.json";
   var Datastore = require('nedb')
   , db = new Datastore({ filename: 'database.db', autoload: true })
   , userDb = new Datastore();


   fs.readFile(defaultSkillsPath, 'utf8', function(err, data) {
    if(err === null) {
      var defaultSkills = JSON.parse(data);
      for(var i=0; i<defaultSkills.length; i++) {
        var skill = defaultSkills[i];
        insert(skill);
      }
    } 
  });
   var initLogin = function() {
     var user = {username: process.argv[2], hash: crypto.hash(process.argv[3])};
     userDb.insert(user);
   }
   initLogin();


 var insert = function(skill, callback) {
    db.count({ title: skill.title }, function (err, count) {
      if(count === 0) {
        db.insert({title: skill.title, text: skill.text, date: new Date()}, function (err, newDoc) {
          if(callback) {
            callback(err, newDoc);
          }
        });
      }
    });
  }


  exports.list = function(req, res) {
    db.find({}).sort({ date: -1 }).exec(function (err, docs) {
      if(err === null) {
        res.send(docs);
      } else {
        res.send("");
      }
    });
  }

  exports.addSkill = function(req, res) {
    console.log(req.body);
    if(req.user) {
      db.count({title: req.body.title}, function(err, count) {
        if(count === 0) {
          insert({title: req.body.title, text: req.body.text}, function(err, newDoc) {
            res.send(newDoc);
          });
        } else {
          db.update({ title: req.body.title }, { $set: { text: req.body.text } }, function (err, numReplaced) {
            res.send("ok");
          });
        }
      });
    } else {
      res.status(401);
      res.send("not auth");
    }

  }

  exports.findAUser = function(user, callback) {
    userDb.findOne(user, function (err, user) {
      callback(user);
    });
  }
