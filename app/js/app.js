'use strict';

/* App Module */

var app = angular.module('portfolio', [
  'ngRoute',
  'ngResource',
  'portfolioAnimations',
  'ui.bootstrap',
  'portfolioControllers',
  'portfolioFilters',
  'portfolioServices'
]);

app.config(['$routeProvider', 
  function($routeProvider) {
    $routeProvider.
      when('/home', {
        templateUrl: 'partials/home.html',
        controller: 'HomeCtrl',
        authenticate: false
      }).
      when('/myProjects', {
        templateUrl: 'partials/myProjects.html',
        controller: 'ProjectsCtrl',
        authenticate: false
      }).
      when('/admin', {
        templateUrl: 'partials/admin.html',
        authenticate: true,
        resolve:{
          'CheckAuth':function(Authentication){
             Authentication.promise;
          }
        }
      }).
      otherwise({
        redirectTo: '/home'
      });
  }]).run( function($rootScope, $location, LoginScreen, Authentication) {

    // register listener to watch route changes
    $rootScope.$on( "$routeChangeStart", function(event, next, current) {
      console.info(Authentication.userIsAuthenticated());
        if ((typeof(next.$$route.authenticate) === "undefined" || next.$$route.authenticate)
          && !Authentication.userIsAuthenticated()) {
              LoginScreen.open();
              $location.path("/home");
        } else {
          // not going to #login, we should redirect now
         
        }
       
    
 });
  })
