'use strict';

/* Controllers */

var controllers = angular.module('portfolioControllers', []);

controllers.controller('HomeCtrl', ['$scope', '$resource',
  function($scope, $resource) {
    var Skills = $resource('/api/skills');
    Skills.query(function(response){
      $scope.skills = response;
    });
  }]);

controllers.controller('ProjectsCtrl', ['$scope', '$routeParams',
  function($scope, $routeParams) {

  }]);

controllers.controller('LoginCtrl', ['$scope', '$http', '$location','LoginScreen', 'Authentication', 'HideAdmin',
  function($scope, $http, $location, LoginScreen, Authentication, HideAdmin) {
    $scope.master = {};
    $scope.user = {};

    $scope.update = function(user) {
      $scope.master = angular.copy(user);
    };

    $scope.reset = function(){
      $scope.user = angular.copy($scope.master);
    };

    $scope.submit = function() {
      $http.post('/login', $scope.user)

      .success(function (data, status, headers, config) {
        LoginScreen.close(); 
        Authentication.setUserAuthenticated();
        $location.path('/admin');
        $scope.reset();

      })

      .error(function() {
        Authentication.setUserNotAuthenticated(); $scope.status='Invalid credentials';
        $scope.reset();
      })};


    }]);

controllers.controller('HeaderCtrl', ['$scope', '$http' ,'$location', '$modal', 'LoginScreen', 'HideAdmin', 'Authentication',
  function ($scope, $http, $location, $modal, LoginScreen, HideAdmin, Authentication) { 
    $scope.isActive = function (viewLocation) { 
      $scope.right = $location.path();
      return viewLocation === $location.path();
    }
    $scope.open = function() {
      LoginScreen.open();
    }

    $scope.hideAdmin=HideAdmin.get();

    $scope.logout = function(){
      $http.get('/logout').success(function (data, status, headers, config) {
        Authentication.setUserNotAuthenticated();
        $location.path('/');
      });

    }

    $scope.$on('HideAdmin.update', function (event, hideAdmin) {
      $scope.hideAdmin = hideAdmin;
    });
    
  }]);


