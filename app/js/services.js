'use strict';

/* Services */

var services = angular.module('portfolioServices', ['ngResource']);


services.service('LoginScreen', ['$modal', 
	function($modal) {
    var modalInstance;
	return {
    close: function() {
      modalInstance.close();
    },
	 open: function () {
     modalInstance = $modal.open({
      templateUrl: 'partials/login.html',
      controller: 'LoginCtrl'
    });
  }
}
}]);

services.factory('HideAdmin', ['$rootScope',function($rootScope){
  var hideAdmin=true;
  return {
    setFalse: function() {
      hideAdmin=false;
      $rootScope.$broadcast('HideAdmin.update', hideAdmin);
    },
    setTrue: function(){
      hideAdmin=true;
      $rootScope.$broadcast('HideAdmin.update', hideAdmin);
    },
    get: function(){
      return hideAdmin;
    }
  }
}])

services.factory('Authentication', ['$http', '$q','HideAdmin', function($http, $q, HideAdmin){
    var deferred = $q.defer();
    var status = false;
    /**
      Init check if we have admin auth
    */
    $http.get('/admin').success(function(data){
      console.info("auth: "+data.access);
      var response = angular.fromJson(data);
      status = response.access;
      deferred.resolve(status);
    });

    var getPromise = function(){
      return deferred.promise;
    }

    return {
      promise:deferred,
    setUserAuthenticated: function() {
      status = true;
    },
    userIsAuthenticated: function(){
      if(status) {HideAdmin.setFalse();}
      else {HideAdmin.setTrue();}
     return status;
   },
   setUserNotAuthenticated: function(){
    status = false;
   }
}
}]);


