# MEAN seed

A MEAN seed I use for webapps.

## Running

start MongoDB:
```
  mongod
  mongo
  use mongo
```
start webbapp:
```
  npm install
  npm start
```

## Testing

### Unit tests

```
npm run test
```

### End to end tests
First follow the steps for running, then:

```
npm run protractor
```
## Rest testing
first follow the steps for running, then:
```
npm run mocha
```
