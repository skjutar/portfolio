
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var api = require('./routes/api')
var http = require('http');
var path = require('path');
var DB = require('./accessDB').AccessDB
var DB = require('./accessDB');
var hash = require('./pass').hash;
var User = require('./models/user');

var app = module.exports = express();

var conn = 'mongodb://localhost/portmgr';
var db;
db = new DB.startup(conn);

app.engine('.html', require('ejs').__express);


app.set('view engine', 'html');

// all environments
app.set('port', process.env.PORT || 80);
app.set('views', path.join(__dirname, 'views'));
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.cookieParser('Cookiiiies '));
app.use(express.session());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));


app.use(function (req, res, next) {
    var err = req.session.error,
        msg = req.session.success;
    delete req.session.error;
    delete req.session.success;
    res.locals.message = '';
    if (err) res.locals.message = '<p class="msg error">' + err + '</p>';
    if (msg) res.locals.message = '<p class="msg success">' + msg + '</p>';
    next();
});

/*
Helper Functions
*/
function authenticate(name, pass, fn) {
    console.log('authenticating %s:%s', name, pass);

 
    DB.getUser(name, function(err, user) {
        if (user) {
            if (err) return fn(new Error('cannot find user'));
            hash(pass, user.salt, function (err, hash) {
                if (err) return fn(err);
                if (hash == user.hash) return fn(null, user);
                fn(new Error('invalid password'));
            });
        } else {
            return fn(new Error('cannot find user'));
        }
    });

}

function requiredAuthentication(req, res, next) {
    if (req.session.user) {
    	console.log("success auth");
    	res.json({access:true});
    } else {
        req.session.error = 'Access denied!';
        console.log("fail auth");
        res.json({access:false});
    }
}

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/api/skills', api.getSkills);



app.post("/login", function (req, res) {
    authenticate(req.body.username, req.body.password, function (err, user) {
        if (user) {
        	console.log("logged in!");
            req.session.regenerate(function () {

                req.session.user = user;
                req.session.success = 'Authenticated as ' + user.username + ' click to <a href="/logout">logout</a>. ' + ' You may now access <a href="/restricted">/restricted</a>.';
                res.json({Login: true});
            });
        } else {
        	console.log("login failed!");
            req.session.error = 'Authentication failed, please check your ' + ' username and password.';
            res.json({Login: false});
        }
    });
});

app.get('/logout', function (req, res) {
    req.session.destroy(function () {
        res.redirect('/');
    });
});

app.get('/admin', requiredAuthentication, function (req, res) {
	console.log("trying to get admin...");
});

/**
	Set up the admin user from users.json
*/

var fs = require('fs');
var file = __dirname + '/users.json';
var jsonData;
fs.readFile(file, 'utf8', function (err, data) {
  if (err) {
    console.log('Following error while reading users: ' + err);
    return;
  }
  jsonData = JSON.parse(data);
  var username = jsonData.username;
  var password = jsonData.password;
  User.remove({}, function(err) { 
   console.log('collection removed') 
   });

    hash(password, function (err, salt, hash) {
        if (err) throw err;
        var user = new User({
            username: username,
            salt: salt,
            hash: hash,
        }).save(function (err, newUser) {
            if (err) throw err;
        });
    });

});





http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
